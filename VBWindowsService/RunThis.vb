﻿Public Class RunThis
    Dim db_mh02 As db_mh02Entities = New db_mh02Entities()
    Dim db_mh02_copy As mh02_copyEntities = New mh02_copyEntities()
    Public p_isRunning As Boolean = False

    Public Sub pb_db_copy()
        While (p_isRunning)
            Dim monitor As List(Of tbl_t_monitor) = db_mh02.tbl_t_monitor.ToList()
            For Each itm In monitor
                Dim find As tbl_t_monitor = db_mh02_copy.tbl_t_monitor.Where(Function(p) p.id = itm.id).SingleOrDefault()
                If find Is Nothing Then
                    db_mh02_copy.tbl_t_monitor.Add(New tbl_t_monitor With {
                            .heart_beat = itm.heart_beat,
                            .timestamp = itm.timestamp,
                            .device_id = itm.device_id,
                            .device_ip = itm.device_ip,
                            .unit_no = itm.unit_no,
                            .incident_code = itm.incident_code,
                            .incident_start = itm.incident_start,
                            .incident_end = itm.incident_end,
                            .is_sound_alert_on = itm.is_sound_alert_on,
                            .is_visual_alert_on = itm.is_visual_alert_on,
                            .image_file_name = itm.image_file_name,
                            .video_file_name = itm.video_file_name,
                            .gps_lat = itm.gps_lat,
                            .gps_long = itm.gps_long,
                            .road_segment_id = itm.road_segment_id,
                            .hm = itm.hm,
                            .rpm = itm.rpm,
                            .vehicle_speed = itm.vehicle_speed,
                            .created_by = itm.created_by,
                            .created_date = itm.created_date,
                            .modified_by = itm.modified_by,
                            .modified_date = itm.modified_date
                        })
                End If
            Next
            Dim message As List(Of tbl_t_message) = db_mh02.tbl_t_message.ToList()
            For Each itm In message
                Dim find As tbl_t_message = db_mh02_copy.tbl_t_message.Where(Function(p) p.id = itm.id).SingleOrDefault()
                If find Is Nothing Then
                    db_mh02_copy.tbl_t_message.Add(New tbl_t_message With {
                            .message = itm.message,
                            .created_date = itm.created_date,
                            .created_by = itm.created_by,
                            .modified_date = itm.modified_date,
                            .modified_by = itm.modified_by,
                            .id_monitor = itm.id_monitor,
                            .incident_code = itm.incident_code,
                            .unit_no = itm.unit_no
                        })
                End If
            Next
            Dim confirmation As List(Of tbl_t_confirmation) = db_mh02.tbl_t_confirmation.ToList()
            For Each itm In confirmation
                Dim find As tbl_t_confirmation = db_mh02_copy.tbl_t_confirmation.Where(Function(p) p.id = itm.id).SingleOrDefault()
                If find Is Nothing Then
                    db_mh02_copy.tbl_t_confirmation.Add(New tbl_t_confirmation With {
                            .confirmation = itm.confirmation,
                            .catatan = itm.catatan,
                            .nama_nrp_gl = itm.nama_nrp_gl,
                            .created_by = itm.created_by,
                            .modified_by = itm.modified_by,
                            .modified_date = itm.modified_date,
                            .created_date = itm.created_date,
                            .id_monitor = itm.id_monitor
                        })
                End If
            Next
            db_mh02_copy.SaveChanges()
            db_mh02_copy.Dispose()
        End While
    End Sub
End Class
