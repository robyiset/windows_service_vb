﻿Imports System.Collections.Specialized
Imports System.ComponentModel
Imports System.Configuration.Install
Imports System.Text
Imports System.Xml

<RunInstaller(True)>
Public Class InstallerSetup
    Inherits Installer

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Overrides Sub Install(stateSaver As IDictionary)
        MyBase.Install(stateSaver)
    End Sub

    Public Overrides Sub Commit(savedState As IDictionary)
        MyBase.Commit(savedState)

        Try
            AddConfigurationFileDetails()
        Catch e As Exception
            MessageBox.Show("Failed : " & e.Message)
            MyBase.Rollback(savedState)
        End Try
    End Sub

    Public Overrides Sub Rollback(savedState As IDictionary)
        MyBase.Rollback(savedState)
    End Sub

    Public Overrides Sub Uninstall(savedState As IDictionary)
        MyBase.Uninstall(savedState)
    End Sub

    Private Sub showParameters()
        Dim sb As StringBuilder = New StringBuilder()
        Dim myStringDictionary As StringDictionary = Me.Context.Parameters

        If Me.Context.Parameters.Count > 0 Then

            For Each myString As String In Me.Context.Parameters.Keys
                sb.AppendFormat("String={0} Value= {1}" & vbLf, myString, Me.Context.Parameters(myString))
            Next
        End If

        MessageBox.Show(sb.ToString())
    End Sub

    Private Sub AddConfigurationFileDetails()
        Try
            Dim TESTPARAMETER As String = Context.Parameters("TESTPARAMETER")
            Dim assemblypath As String = Context.Parameters("assemblypath")
            Dim appConfigPath As String = assemblypath & ".config"
            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(appConfigPath)
            Dim configuration As XmlNode = Nothing

            For Each node As XmlNode In doc.ChildNodes
                If node.Name = "configuration" Then configuration = node
            Next

            If configuration IsNot Nothing Then
                Dim settingNode As XmlNode = Nothing

                For Each node As XmlNode In configuration.ChildNodes
                    If node.Name = "appSettings" Then settingNode = node
                Next

                If settingNode IsNot Nothing Then

                    For Each node As XmlNode In settingNode.ChildNodes
                        If node.Attributes Is Nothing Then Continue For
                        Dim attribute As XmlAttribute = node.Attributes("value")

                        If node.Attributes("key") IsNot Nothing Then

                            Select Case node.Attributes("key").Value
                                Case "TestParameter"
                                    attribute.Value = TESTPARAMETER
                            End Select
                        End If
                    Next
                End If

                doc.Save(appConfigPath)
            End If

        Catch
            Throw
        End Try
    End Sub

End Class
