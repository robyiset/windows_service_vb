﻿Imports System.Threading
Public Class Service1

    Dim rt As RunThis = New RunThis()
    Dim th As Thread
    Protected Overrides Sub OnStart(args() As String)
        th = New Thread(AddressOf rt.pb_db_copy)
        th.Start()
        rt.p_isRunning = True
    End Sub

    Protected Overrides Sub OnStop()
        rt.p_isRunning = False
        th.Abort()
    End Sub
End Class
