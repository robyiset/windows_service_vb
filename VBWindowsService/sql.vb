﻿Imports System.Data.SqlClient
Public Class sql
    Private i_sqCon As New SqlConnection(Configuration.ConfigurationManager.ConnectionStrings("SqlServerConnection").ConnectionString)
    Private i_sqCmd As New SqlCommand
    Private i_sdrRow As SqlDataReader

    Public p_isRunning As Boolean = False

    Private i_schema_selected() As String
    Private i_view_name() As String

    Private i_db_from As String = Configuration.ConfigurationManager.AppSettings("db_from").ToString()
    Private i_db_to As String = Configuration.ConfigurationManager.AppSettings("db_to").ToString()

    Private Function pv_cmd(comm As String) As SqlCommand
        i_sqCmd.Connection = i_sqCon
        i_sqCmd.CommandText = comm
        Return i_sqCmd
    End Function
    Public Sub pb_db_copy()
        Dim i As Int32 = 0
        Dim total As Int32 = 0
        While (p_isRunning)
            i_sqCon.Open()
            i_sdrRow = pv_cmd("SELECT count(name) as found FROM sys.databases where name = '" + i_db_to + "'").ExecuteReader()
            If (Integer.Parse(i_sdrRow.GetValue(0)) & Convert.ToInt32(i_sdrRow.GetValue(0) > 0)) Then
                i_sdrRow.Close()
            Else
                i_sdrRow.Close()
                pv_cmd("CREATE DATABASE " + i_db_to).ExecuteNonQuery()
            End If
            'table copy
            i_sdrRow = pv_cmd("SELECT count(TABLE_NAME) as total FROM " + i_db_from + ".INFORMATION_SCHEMA.TABLES").ExecuteReader()
            For Each itm In i_sdrRow
                total = Convert.ToInt32(i_sdrRow.GetValue(0))
            Next
            i_sdrRow.Close()
            If (total > 0) Then
                i_schema_selected = New String(total - 1) {}
                i_sdrRow = pv_cmd("SELECT TABLE_NAME FROM " + i_db_from + ".INFORMATION_SCHEMA.TABLES").ExecuteReader()
                For Each itm In i_sdrRow
                    i_schema_selected(i) = i_sdrRow.GetValue(0)
                    i += 1
                Next
                i_sdrRow.Close()
                For Each tbl In i_schema_selected
                    i_sdrRow = pv_cmd("SELECT count(TABLE_NAME) as found FROM " + i_db_to + ".INFORMATION_SCHEMA.TABLES where TABLE_NAME = '" + tbl + "'").ExecuteReader()
                    For Each itm In i_sdrRow
                        If (Integer.Parse(i_sdrRow.GetValue(0)) & Convert.ToInt32(i_sdrRow.GetValue(0) > 0)) Then

                        Else
                            i_sdrRow.Close()
                            pv_cmd("select * into " + i_db_to + ".dbo." + tbl + " from " + i_db_from + ".dbo." + tbl).ExecuteNonQuery()
                            i_sdrRow.Close()
                        End If
                    Next
                Next
                i_sdrRow.Close()
            End If

            'view copy
            i = 0
            i_sdrRow = pv_cmd("Select count(VIEW_DEFINITION) as total FROM " + i_db_from + ".INFORMATION_SCHEMA.VIEWS").ExecuteReader()
            For Each itm In i_sdrRow
                total = Convert.ToInt32(i_sdrRow.GetValue(0))
            Next
            i_sdrRow.Close()
            If (total > 0) Then
                i_schema_selected = New String(i_sdrRow.GetValue(0) - 1) {}
                i_view_name = New String(i_sdrRow.GetValue(0) - 1) {}
                i_sdrRow = pv_cmd("Select TABLE_NAME, VIEW_DEFINITION FROM " + i_db_from + ".INFORMATION_SCHEMA.VIEWS").ExecuteReader()
                For Each itm In i_sdrRow
                    i_view_name(i) = i_sdrRow.GetValue(0)
                    i_schema_selected(i) = i_sdrRow.GetValue(1)
                    i += 1
                Next
                i_sdrRow.Close()
                pv_cmd("use " + i_db_to).ExecuteNonQuery()
                i_sdrRow.Close()
                For j As Integer = 0 To i_schema_selected.Length - 1 Step 1
                    i_sdrRow = pv_cmd("SELECT count(TABLE_NAME) as found FROM " + i_db_to + ".INFORMATION_SCHEMA.VIEWS where TABLE_NAME = '" + i_view_name(j) + "'").ExecuteReader()
                    For Each itm In i_sdrRow
                        If (Integer.Parse(i_sdrRow.GetValue(0)) & Convert.ToInt32(i_sdrRow.GetValue(0) > 0)) Then
                            i_sdrRow.Close()
                        Else
                            i_sdrRow.Close()
                            pv_cmd(i_schema_selected(j)).ExecuteNonQuery()
                            i_sdrRow.Close()
                        End If
                    Next
                Next
                pv_cmd("use " + i_db_from).ExecuteNonQuery()
                i_sdrRow.Close()
            End If
            i_sqCon.Close()

            i = 0
            total = 0
        End While

    End Sub
End Class
